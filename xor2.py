import argparse
import pickle

import gurobipy as gp


class Solver:

    def __init__(self, target, dims, num, depth, branch, dbn, lbn):
        self.dims = dims
        self.target = target
        if target:
            self.target_matrix = self._parse_matrix(target)
        self.num = num
        self.depth = depth
        self.branch = branch
        self.lbn = lbn
        self.dbn = dbn
        self.mats = []
        self.model = gp.Model()
        self.vars = {}
        self.t_cnt = 0
        self.obj = 0
        self.LARGE = 2 * self.num

    def _create_var(self, name):
        self.vars[name] = self.model.addVar(vtype=gp.GRB.BINARY, name=name)
    
    def _create_var_int(self, name):
        self.vars[name] = self.model.addVar(lb=0, vtype=gp.GRB.INTEGER, name=name)
    
    def _init_matrices(self):

        for i in range(self.num):
            mat = []
            for j in range(self.dims):
                row = []
                for k in range(self.dims):
                    var = "x_{}_{}_{}".format(i,j,k)
                    self._create_var(var)
                    row.append(var)
                mat.append(row)
            self.mats.append(mat)

    def _init_matrices_depth(self):

        for i in range(self.num):
            mat = []
            for j in range(self.dims):
                row = []
                depth = "d_{}_{}".format(j,i)
                self._create_var_int(depth)
                if i == 0:
                    self.model.addConstr(self.vars[depth] == 0)
                else:
                    self.model.addConstr(self.vars[depth] >= self.vars["d_{}_{}".format(j,i-1)])
                for k in range(self.dims):
                    var = "x_{}_{}_{}".format(i,j,k)
                    self._create_var(var)
                    row.append(var)
                mat.append(row)
            self.mats.append(mat)

    # First - Permutation
    # Rest - Addition
    def _init_constraints(self):

        for i in range(self.dims):
            row = 0
            col = 0
            for j in range(self.dims):
                row += self.vars[self.mats[0][i][j]]
                col += self.vars[self.mats[0][j][i]]
            self.model.addConstr(row == 1)
            self.model.addConstr(col == 1)
        
        for i in range(1, self.num):
            q = 0
            for j in range(self.dims):
                for k in range(self.dims):
                    if j == k:
                        self.model.addConstr(self.vars[self.mats[i][j][k]] == 1)
                    else:
                        q += self.vars[self.mats[i][j][k]]
            self.model.addConstr(q == 1)

    # First - Permutation
    # Rest - Addition
    def _init_constraints_depth(self):

        for i in range(self.dims):
            row = 0
            col = 0
            for j in range(self.dims):
                row += self.vars[self.mats[0][i][j]]
                col += self.vars[self.mats[0][j][i]]
            self.model.addConstr(row == 1)
            self.model.addConstr(col == 1)

        depths = []
        for i in range(1, self.num):
            q = 0
            for j in range(self.dims):
                for k in range(self.dims):
                    if j == k:
                        self.model.addConstr(self.vars[self.mats[i][j][k]] == 1)
                    else:
                        depth_j = "d_{}_{}".format(j, i-1)
                        depth_k = "d_{}_{}".format(k, i-1)
                        depth = "d_{}_{}".format(j, i)
                        temp = "t_{}_{}_{}".format(j, k, i)
                        # temp = "tempvar_{}".format(self.t_cnt)
                        # self.t_cnt += 1
                        self._create_var_int(temp)
                        self.model.addGenConstrMax(self.vars[temp], [self.vars[depth_j], self.vars[depth_k]])
                        self.model.addConstr(self.LARGE * (self.vars[self.mats[i][j][k]] - 1) <= 
                                                self.vars[depth] - self.vars[temp] - 1)
                        q += self.vars[self.mats[i][j][k]]
                if i == self.num - 1:
                    depth = "d_{}_{}".format(j, i)
                    depths.append(self.vars[depth])
            self.model.addConstr(q == 1)
        
        self._create_var_int("obj")
        self.model.addGenConstrMax(self.vars["obj"], depths)
        self.obj = self.vars["obj"]

            
    def _parse_matrix(self, target):

        with open(target, "r") as f:
            target_matrix = [list(map(int, line.strip().split())) for line in f]

        return target_matrix
    
    def _matrix_multiply(self, mat1, mat2):

        newmat = []

        for i in range(self.dims):
            row = []
            for j in range(self.dims):
                temp = 0
                temp_var = "t_{}".format(self.t_cnt)
                self.t_cnt += 1
                self._create_var_int(temp_var)
                for k in range(self.dims):
                    t_var = "t_{}".format(self.t_cnt)
                    self._create_var_int(t_var)
                    self.model.addGenConstrAnd(self.vars[t_var], [self.vars[mat1[i][k]], 
                                                                self.vars[mat2[k][j]]])
                    temp += self.vars[t_var]
                    self.t_cnt += 1
                self.model.addConstr(self.vars[temp_var] == temp)
                row.append(temp_var)
            newmat.append(row)

        return newmat

    def solve(self):

        if self.depth:
            self._init_matrices_depth()
            self._init_constraints_depth()
            mat = self.mats[0]
            for i in range(1, self.num):
                mat = self._matrix_multiply(self.mats[i], mat)

        else:
            self._init_matrices()
            self._init_constraints()
            mat = self.mats[0]
            for i in range(1, self.num):
                mat = self._matrix_multiply(mat, self.mats[i])

        if self.branch:
            width = self.dims
            n = 2**self.dims

            for i in range(1,n):
                v = list(map(int, f'{i:0{width}b}'))
                
                m_dbn, m_lbn = [], []
                for j in range(self.dims):
                    r_dbn, r_lbn = 0, 0;
                    for k in range(self.dims):
                        r_dbn += self.vars[mat[j][k]] * v[k]
                        r_lbn += self.vars[mat[k][j]] * v[k]
                    m_dbn.append(r_dbn)
                    m_lbn.append(r_lbn)

                if self.dbn:
                    self.model.addConstr(sum(v) + sum(m_dbn) >= self.dbn)
                if self.lbn:
                    self.model.addConstr(sum(v) + sum(m_lbn) >= self.lbn)
        else:
            for i in range(self.dims):
                for j in range(self.dims):
                    t_var = "f_{}_{}".format(i,j)
                    self._create_var_int(t_var)
                    self.model.addConstr(self.vars[mat[i][j]] == 2 * self.vars[t_var] + self.target_matrix[i][j])

        self.model.setObjective(self.obj, gp.GRB.MINIMIZE)
        self.model.optimize()

        if self.model.status == gp.GRB.OPTIMAL:
            print ("Solution found at {} factor matrices".format(self.num))
            solution = []
            for i in range(self.num):
                temp = []
                for j in range(self.dims):
                    row = []
                    for k in range(self.dims):
                        row.append(int(self.vars[self.mats[i][j][k]].x))
                    temp.append(row)
                solution.append(temp)
            
            if self.branch:
                target = []
                for i in range(self.dims):
                    row = []
                    for j in range(self.dims):
                        row.append(int(self.vars[mat[i][j]].x) % 2)
                    target.append(row)
                solution.append(target)

            
            with open("{}_{}_{}.pkl".format(self.target, self.dims, self.num), "wb+") as f:
                pickle.dump(solution, f)

            return True
        
        print ("No solution at {} factor matrices".format(self.num))
        
        return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MILP based factoring')
    parser.add_argument('-target', type=str, help='Path of target matrix (*.txt)')
    parser.add_argument('-dims', type=int, help='Dimensions of target matrix', required=True)
    parser.add_argument('-num', type=int, help='Number of factor matrices', required=True)
    parser.add_argument('-depth', help='Disable depth optimization', action='store_false')
    parser.add_argument('-branch', help='Model branch number', action='store_true')
    parser.add_argument('-dbn', type=int, help='Add mds constraints')
    parser.add_argument('-lbn', type=int, help='Add mds constraints')
    
    args = parser.parse_args()

    if args.branch is False:
        if args.dbn or args.lbn:
            raise parser.error('-lbn/-dbn requires -branch')
        if args.target is None:
            raise parser.error('-target is required')
    else:
        if args.target:
            raise parser.error('-target and -branch should be not be set together')
        if not args.dbn and not args.lbn:
            raise parser.error('Choose -lbn/-dbn')

    num = args.num
    depth = 0

    while True:
        obj = Solver(args.target, args.dims, num, args.depth, args.branch, args.dbn, args.lbn)
        if obj.solve():
            if args.depth:
                depth = obj.vars["obj"].x
            break
        num += 1

    with open("{}_{}_{}.pkl".format(args.target, args.dims, num), "rb") as f:
        solution = pickle.load(f)

    if args.depth:
        for i in range(num-1,-1,-1):
            print (num-i)
            for j in range(args.dims):
                for k in range(args.dims):
                    print (solution[i][j][k], end=" ")
                print()
    else:
        for i in range(num):
            print (i+1)
            for j in range(args.dims):
                for k in range(args.dims):
                    print (solution[i][j][k], end=" ")
                print()
    
    if args.branch:
        print ("Target: ")
        for i in range(args.dims):
            for j in range(args.dims):
                print (solution[num][i][j], end=" ")
            print()

    if depth != 0:
        print ("Depth: ", int(depth))