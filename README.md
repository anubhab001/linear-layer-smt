# linear-layer-smt

The repository the contains implementation of the paper [**Optimizing Device Implementation of Linear Layers with Automated Tools**](https://www.springer.com/gp/book/9783030816445) by [Anubhab Baksi](mailto:anubhab001@e.ntu.edu.sg), [Banashri Karmakar](mailto:banashrik@iitbhilai.ac.in) and [Vishnu Asutosh Dasu](mailto:vishnu.dasu1@tcs.com).

The tool requires [Gurobi](https://www.gurobi.com/) and [`gurobipy`](https://www.gurobi.com/documentation/9.1/quickstart_mac/cs_grbpy_the_gurobi_python.html).

## Files

1. `xor2.py` - Generate matrix implementation using XOR2 gates.
2. `xor3.py` - Generate matrix implementation using XOR2 and XOR3 gates.

## Usage

The following command generates the implementation of the `7x7` Lucas matrix using XOR2 gates and `10` factor matrices.

`python3 xor.py -t lucas_7by7.txt -dims 7 -n 10`

## Input format

The following is an example of the Lucas matrix in `lucas_7by7.txt`:
```
    1 1 0 0 0 0 0
    0 1 1 0 0 0 0
    0 0 1 1 0 0 0
    0 0 0 1 1 0 0
    0 0 0 0 1 1 0
    0 0 0 0 0 1 1
    1 0 0 1 0 0 1
```