import argparse
import pickle

import gurobipy as gp
import numpy as np

class Solver:

    def __init__(self, target, dims, num, xor2, xor3, proto):
        self.dims = dims
        self.target = target
        self.target_matrix = self._parse_matrix(target)
        self.num = num
        self.xor2 = xor2
        self.xor3 = xor3
        self.proto = proto
        self.name = "{}_{}_{}".format(self.target, self.dims, self.num)
        self.mats = []
        self.model = gp.Model()
        self.model.setParam('LogFile', self.name+".log")
        self.model.setParam('OutputFlag', 0)
        self.vars = {}
        self.t_cnt = 0
        self.obj = 0

    def _create_var(self, name):
        self.vars[name] = self.model.addVar(vtype=gp.GRB.BINARY, name=name)
    
    def _create_var_int(self, name):
        self.vars[name] = self.model.addVar(lb=0, vtype=gp.GRB.INTEGER, name=name)
    
    def _init_matrices(self):

        for i in range(self.num):
            mat = []
            for j in range(self.dims):
                row = []
                for k in range(self.dims):
                    var = "x_{}_{}_{}".format(i,j,k)
                    self._create_var(var)
                    row.append(var)
                mat.append(row)
            self.mats.append(mat)

    def _init_constraints(self):

        # Permutation
        for i in range(self.dims):
            row = 0
            col = 0
            for j in range(self.dims):
                row += self.vars[self.mats[0][i][j]]
                col += self.vars[self.mats[0][j][i]]
            self.model.addConstr(row == 1)
            self.model.addConstr(col == 1)

        for i in range(1, self.num):
            q_2 = "q_{}_2".format(i)    # XOR2 constraint
            q_3 = "q_{}_3".format(i)    # XOR3 constraint
            self._create_var(q_2)
            self._create_var(q_3)
            # Permutation/XOR2/XOR3
            self.model.addConstr(self.vars[q_2] + self.vars[q_3] == 1)      # 1
            matsum = 0
            mat3sum = 0
            for j in range(self.dims):
                rowsum = 0
                r_3 = "r_{}_{}".format(i,j)     # Row condition for XOR3
                self._create_var_int(r_3)
                for k in range(self.dims):
                    if j == k:      # All matrix diagonal is 1
                        self.model.addConstr(self.vars[self.mats[i][j][k]] == 1)
                    else:
                        rowsum += self.vars[self.mats[i][j][k]]    
                self.model.addConstr(rowsum <= 2 * self.vars[r_3] + self.vars[q_2])   # 3c
                mat3sum += self.vars[r_3]
                matsum += rowsum
            self.model.addConstr(mat3sum == self.vars[q_3])    # 3b
            self.model.addConstr(matsum == self.vars[q_2] + 2*self.vars[q_3])   # 2
            self.obj += (self.xor2 * self.vars[q_2] + self.xor3 * self.vars[q_3])  # 4
                    
            
    def _parse_matrix(self, target):

        with open(target, "r") as f:
            target_matrix = [list(map(int, line.strip().split())) for line in f]

        return target_matrix
    
    def _matrix_multiply(self, mat1, mat2):

        newmat = []

        for i in range(self.dims):
            row = []
            for j in range(self.dims):
                temp = 0
                temp_var = "t_{}".format(self.t_cnt)
                self.t_cnt += 1
                self._create_var_int(temp_var)
                for k in range(self.dims):
                    t_var = "t_{}".format(self.t_cnt)
                    self._create_var_int(t_var)
                    self.model.addGenConstrAnd(self.vars[t_var], [self.vars[mat1[i][k]], 
                                                                self.vars[mat2[k][j]]])
                    temp += self.vars[t_var]
                    self.t_cnt += 1
                self.model.addConstr(self.vars[temp_var] == temp)
                row.append(temp_var)
            newmat.append(row)

        return newmat

    def solve(self):
        
        self._init_matrices()

        self._init_constraints()

        mat = self.mats[0]
        for i in range(1, self.num):
            mat = self._matrix_multiply(mat, self.mats[i])
        
        for i in range(self.dims):
            for j in range(self.dims):
                t_var = "f_{}_{}".format(i,j)
                self._create_var_int(t_var)
                self.model.addConstr(self.vars[mat[i][j]] == 2 * self.vars[t_var] + self.target_matrix[i][j])

        self.model.setObjective(self.obj, gp.GRB.MINIMIZE)
        self.model.optimize()

        if self.model.status == gp.GRB.OPTIMAL:
            print ("Solution found at {} factor matrices".format(self.num))
            solution = []
            for i in range(self.num):
                mat = []
                for j in range(self.dims):
                    row = []
                    for k in range(self.dims):
                        row.append(int(self.vars[self.mats[i][j][k]].x))
                    mat.append(row)
                solution.append(mat)
            
            with open(self.name + ".pkl", "wb+") as f:
                pickle.dump(solution, f, protocol=self.proto)
            
            self.model.write(self.name + ".sol")

            return True
        
        print ("No solution at {} factor matrices".format(self.num))
        
        return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MILP based factoring')
    parser.add_argument('-target', '-t', type=str, help='Path of target matrix (*.txt)', required=True)
    parser.add_argument('-dims', type=int, help='Dimensions of target matrix', required=True)
    parser.add_argument('-num', type=int, help='Number of factor matrices', required=True)
    parser.add_argument('-proto', type=int, help='Pickle protocol', default=pickle.HIGHEST_PROTOCOL)
    parser.add_argument('-xor2', type=float, help='XOR2 cost', default=1.0)
    parser.add_argument('-xor3', type=float, help='XOR2 cost', default=1.625)
    
    args = parser.parse_args()
    num = args.num

    cost = -1
    while True:
        obj = Solver(args.target, args.dims, num, args.xor2, args.xor3, args.proto)
        if obj.solve():
            cost = obj.model.objval
            break
        num += 1
    
    with open("{}_{}_{}.pkl".format(args.target, args.dims, num), "rb") as f:
        solution = pickle.load(f)
    
    for i in range(num):
        print (i+1)
        for j in range(args.dims):
            for k in range(args.dims):
                print (solution[i][j][k], end=" ")
            print()

    print ("Cost: ", cost)
